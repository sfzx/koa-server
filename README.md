# Installation and usage instructions

## Node

### You need to have Node installed on your machine to be able to you Node.js and npm

### Building and running server

Install all dependencies with 
```bash
npm install
```
Now you are ready to go

Building project for production 
```bash
npm run build
```

Start server for production
```bash
npm run start
```
In this way you will need to rebuild and restart server after any changes in code

Run watch mode for development
```bash
npm run watch
```
In this way server will automatically restarted after any changes in files

To run linter
```bash
npm run lint
```

To run fix with linter
```bash
npm run fix
``` 


# Postman documentation

[link](https://documenter.getpostman.com/view/12458539/2s8YmSr16W)
