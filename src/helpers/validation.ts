import * as Joi from 'joi';
import jwt from 'jsonwebtoken';

import { JWT_KEY } from '../config/config';
import { ROLES } from '../db/config';

export const validateBody = async (
  ctx,
  schema: Joi.ObjectSchema<any> | Joi.StringSchema,
  next
) => {
  const data = ctx.request.body;
  if (!data || (typeof data === 'object' && !Object.keys(data).length)) {
    ctx.throw(400, 'No data');
  }

  const validationResult = schema.validate(data);
  if (validationResult.error) {
    ctx.throw(422, validationResult.error.message);
  }
};

export const validateAuthorization = async (
  ctx,
  { role, id }: { role?: ROLES; id?: string },
  next
) => {
  try {
    const token = ctx.request.header.authorization.split(' ')[1];
    const decoded = jwt.verify(token, JWT_KEY);

    if (role && decoded['role'] !== role)
      ctx.throw(401, `Role should equal to ${role}`);
    if (id && decoded['id'] !== id) ctx.throw(401, `Id should equal to ${id}`);

    ctx.request.decodedToken = decoded;
  } catch (error) {
    ctx.throw(401, 'Not valid token');
  }
};
