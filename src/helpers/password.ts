import * as aes256 from 'aes256';
import { ENCRYPTION_KEY } from '../config/config';

export const encrypt = (text: string): string => {
  return aes256.encrypt(ENCRYPTION_KEY, text);
};

export const decrypt = (text: string): string => {
  return aes256.decrypt(ENCRYPTION_KEY, text);
};
