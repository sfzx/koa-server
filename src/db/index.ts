import mongoose, { Connection } from 'mongoose';

export default (uri: string): Promise<Connection> =>
  new Promise((resolve, reject) => {
    mongoose.connection
      .on('error', (error) => {
        console.log('Error: ', error);
        return reject(error);
      })
      .on('close', () => console.log('Database connection closed.'))
      .once('open', () => resolve(mongoose.connections[0]));

    mongoose.connect(uri, { dbName: 'koaDb' });
  });
