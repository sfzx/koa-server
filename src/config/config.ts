export const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY || 'testKey';
export const JWT_KEY = 'MyJWTKey';
export const TOKEN_EXPIRATION_TIME = '1d';
