import { CustomJoi } from '../data/customJoi';
import { ROLES } from '../db/config';
import { validateAuthorization, validateBody } from '../helpers/validation';

const emailScheme = CustomJoi.string().email().required();

const createUserSchema = CustomJoi.object({
  firstName: CustomJoi.string(),
  lastName: CustomJoi.string(),
  email: emailScheme,
  password: CustomJoi.string(),
  country: CustomJoi.string(),
  city: CustomJoi.string(),
  address: CustomJoi.string(),
  postalCode: CustomJoi.string(),
  phoneNumber: CustomJoi.string().phoneNumber(),
  gender: CustomJoi.string().allow('male', 'female').only(),
  agreement: CustomJoi.boolean().allow(true),
}).presence('required');

const updateUserSchema = createUserSchema.presence('optional');

const loginSchema = CustomJoi.object({
  email: emailScheme,
  password: CustomJoi.string(),
});

export const createUserValidate = async (ctx, next) => {
  await validateBody(ctx, createUserSchema, next);
  await next();
};

export const updateUserValidate = async (ctx, next) => {
  await validateBody(ctx, updateUserSchema, next);
  await validateAuthorization(
    ctx,
    { role: ROLES.USER, id: ctx.request.params.userId },
    next
  );
  await next();
};

export const getUsersValidate = async (ctx, next) => {
  await validateAuthorization(ctx, { role: ROLES.USER }, next);
  await next();
};

export const emailValidate = async (ctx, next) => {
  await validateBody(ctx, CustomJoi.object({ email: emailScheme }), next);
  await next();
};

export const loginValidate = async (ctx, next) => {
  await validateBody(ctx, loginSchema, next);
  await next();
};

export const getUserByIdValidate = async (ctx, next) => {
  await next();
};
