import jwt from 'jsonwebtoken';

import { JWT_KEY, TOKEN_EXPIRATION_TIME } from '../config/config';
import { ROLES } from '../db/config';
import { decrypt, encrypt } from '../helpers/password';
import User from './auth.model';

export default {
  create: async (userData) => {
    const user = await User.create({
      ...userData,
      password: encrypt(userData.password),
    });
    return user.toJSON();
  },
  getAll: () => {
    return User.find({}, { password: 0 });
  },
  getBy: async (filter: object) => {
    const user = await User.findOne(filter);
    return user?.toJSON() || user;
  },
  getById: async (id: string) => {
    // Users by id shouldn't return all user data
    const user = (await User.findById(id)).toJSON();
    const returnUser = (({ password, agreement, ...user }) => user)(user);
    return returnUser;
  },
  update: async (filter: object, newData) => {
    return User.findOneAndUpdate(filter, newData, {
      new: true,
    });
  },
  login: async (password: string, user) => {
    if (decrypt(user.password) === password) {
      const userData = (({ password, id, ...user }) => user)(user);
      const cookieData = { ...userData, id: user.id, role: ROLES.USER };
      return jwt.sign(cookieData, JWT_KEY, {
        expiresIn: TOKEN_EXPIRATION_TIME,
      });
    } else {
      return false;
    }
  },
  getUserByToken: (token: string) => {
    try {
      const decoded = jwt.verify(token, JWT_KEY);
      return decoded;
    } catch (error) {
      return null;
    }
  },
};
