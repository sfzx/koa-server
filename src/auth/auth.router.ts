import Router from 'koa-router';

import userService from './auth.service';
import {
  createUserValidate,
  emailValidate,
  getUserByIdValidate,
  getUsersValidate,
  loginValidate,
  updateUserValidate,
} from './auth.validation';

export const createUser = async (ctx, next: () => Promise<any>) => {
  const user = await userService.getBy({ email: ctx.request.body.email });
  if (user) {
    return ctx.throw(403, 'User with such email already exist');
  }

  const newUser = await userService.create(ctx.request.body);
  if (!newUser) {
    return ctx.throw(422, 'Crashed');
  }

  ctx.body = {
    user: (({ password, ...user }) => user)(newUser), //Exclude password,
    token: await userService.login(ctx.request.body.password, newUser),
  };
  ctx.response.status = 201;
};

const getUser = async (ctx, next) => {
  const userToken = ctx.headers.authorization;
  if (!userToken) {
    return ctx.throw(422, 'Crashed');
  }

  const token = userToken.split(' ')[1];
  const user = userService.getUserByToken(token);

  ctx.response.status = 200;
  if (!user) {
    ctx.body = null;
  }

  ctx.body = user;
};

const updateUser = async (ctx, next: () => Promise<any>) => {
  const user = await userService.update(
    { _id: ctx.params.userId },
    ctx.request.body
  );
  if (!user) {
    return ctx.throw(404, 'No user with such id');
  }

  ctx.body = user;
  ctx.response.status = 200;
};

const getAllUsers = async (ctx, next: () => Promise<any>) => {
  const users = await userService.getAll();
  ctx.body = users;
  ctx.response.status = 200;
};

const checkEmail = async (ctx, next) => {
  const user = await userService.getBy({ email: ctx.request.body.email });
  const errors = {};

  if (user) {
    // Structure to use with formik errors
    // errors.push({
    //   errorMessage: 'User with such email already exist',
    //   field: 'email',
    // });
    errors['email'] = 'User with such email already exist';
  }

  if (Object.keys(errors).length > 0) {
    ctx.response.status = 422;
    return (ctx.response.body = {
      uniq: false,
      message: 'User signup failed due to validation errors.',
      errors,
    });
  }

  ctx.body = {
    uniq: true,
  };
  ctx.response.status = 201;
};

const login = async (ctx, next) => {
  const user = await userService.getBy({ email: ctx.request.body.email });
  const errors = {};

  if (!user) {
    errors['email'] = 'Incorrect credentials';
    errors['password'] = 'Incorrect credentials';
    ctx.response.status = 422;
    return (ctx.response.body = {
      message: 'User login failed due to validation errors.',
      errors,
    });
  }

  const token = await userService.login(ctx.request.body.password, user);

  if (!token) {
    errors['email'] = 'Incorrect credentials';
    errors['password'] = 'Incorrect credentials';
  }

  if (Object.keys(errors).length > 0) {
    ctx.response.status = 422;
    return (ctx.response.body = {
      message: 'User login failed due to validation errors.',
      errors,
    });
  }

  ctx.body = {
    user: (({ password, ...user }) => user)(user), //Exclude password
    token,
  };
  ctx.response.status = 201;
};

const getUserById = async (ctx, next) => {
  const userId = ctx.params?.userId;
  if (!userId) {
    return ctx.throw(422, 'Invalid data');
  }

  const user = await userService.getById(userId);

  ctx.response.status = 200;
  ctx.body = user;
};

const router = new Router();

router.post('/user', createUserValidate, createUser);
router.get('/user', getUsersValidate, getUser);
router.get('/user/:userId', getUserByIdValidate, getUserById);
router.post('/email', emailValidate, checkEmail);
router.post('/login', loginValidate, login);
router.put('/user/:userId/update', updateUserValidate, updateUser);
router.get('/all', getUsersValidate, getAllUsers);

export default router;
