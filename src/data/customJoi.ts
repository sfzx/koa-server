import Joi from 'joi';
import joiPhone from 'joi-phone-number';

export const CustomJoi: Joi.Root = Joi.extend(joiPhone);
