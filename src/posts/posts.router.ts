import Router from 'koa-router';

import postsService from './posts.service';

const sendPosts = async (ctx, next) => {
  const ownerId = ctx?.params?.ownerId || null;
  const postId = ctx?.params?.postId || null;
  let posts = null;
  if (ownerId && postId) {
    posts = await postsService.getBy({ _id: postId });
  } else if (ownerId) {
    posts = await postsService.getAll({ owner: ownerId });
  } else {
    posts = await postsService.getAll();
  }
  ctx.body = posts || [];
  ctx.response.status = 200;
};

const createPost = async (ctx, next) => {
  const postBody = ctx.request.body;
  if (!postBody) {
    return ctx.throw(422, 'Invalid data');
  }

  const postId = ctx?.params?.postId || null;
  let newPost;

  if (postId) {
    newPost = await postsService.update({ _id: postId }, postBody);
  } else {
    newPost = await postsService.create(postBody);
  }

  if (!newPost) {
    return ctx.throw(422, 'Crashed');
  }

  // const posts = await postsService.getAll();

  ctx.body = {
    success: true,
    post: newPost,
  };
};

const deletePost = async (ctx, next) => {
  const postId = ctx?.params?.postId || null;

  const newPosts = await postsService.delete({ _id: postId });

  if (!newPosts) {
    return ctx.throw(422, 'Crashed');
  }

  ctx.body = {
    success: true,
    posts: newPosts,
  };
};

const router = new Router({ prefix: '/posts' });

router.get('/', sendPosts);
router.get('/:ownerId', sendPosts);
router.get('/:ownerId/:postId', sendPosts);
router.post('/', createPost);
router.put('/:postId', createPost);
router.delete('/:postId', deletePost);

export default router;
