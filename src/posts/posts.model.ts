import mongoose, { Schema } from 'mongoose';

const PostsSchema = new Schema(
  {
    owner: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
      require: true,
    },
    title: {
      type: String,
      require: true,
    },
    image: {
      type: String,
      require: true,
    },
    article: {
      type: String,
      require: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
  }
);

export default mongoose.model('Posts', PostsSchema);
