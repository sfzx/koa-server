import Posts from './posts.model';

export default {
  getAll: (filter: {} = {}) => {
    return Posts.find(filter);
  },
  getBy: async (filter: object) => {
    const post = await Posts.findOne(filter);
    return post;
  },
  create: (post: object) => {
    return Posts.create(post);
  },
  update: (filter: object, post: object) => {
    return Posts.findByIdAndUpdate(filter, post, {
      new: true,
    });
  },
  delete: async (filter: object) => {
    const posts = await Posts.findOneAndDelete(filter, { new: true });
    return posts.toJSON();
  },
};
