import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import json from 'koa-json';
import logger from 'koa-logger';
import Router from 'koa-router';

import cors from '@koa/cors';

import auth from './auth/auth.router';
import connectDatabase from './db';
import { development, production } from './db/config';
import { errorHandler } from './helpers/errorHandler';
import posts from './posts/posts.router';

const port = process.env.PORT || 4000;
const databaseConfig =
  process.env.NODE_ENV === 'production' ? production : development;

const app = new Koa();

(async () => {
  app.use(cors());
  app.use(logger());
  app.use(json());
  app.use(bodyParser());
  app.use(errorHandler);

  try {
    const info = await connectDatabase(databaseConfig);
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
  } catch (error) {
    console.error('Unable to connect to database');
  }

  const router = new Router({ prefix: '/api' });

  router.use(auth.routes());
  router.use(posts.routes());

  app.use(router.routes());
  app.listen(port, () => {
    console.log(`Server started on port ${port}`);
  });
})();
